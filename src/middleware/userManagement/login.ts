import { AnyAction } from "redux";
import { ThunkAction } from "redux-thunk";
import { LOGIN_USER, LOGIN_ROLE } from "../../redux/types/actInTypes";

const dateNow = new Date();

interface UserData {
    email : string;
    password : string;
    username? : string;
}
export const loginDataUser = (data : UserData) : ThunkAction<Promise<void>, {}, {}, AnyAction> => async dispatch => {

    let dataUser = {...data}
    dispatch({
        type : LOGIN_USER,
        Payload : dataUser
    })

}