import React, {Suspense} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import HeaderLayout from './component/HeaderLayout';

import routes from '../routes';
import { Route, Routes } from 'react-router-dom';

const LoaderPage = () => <div>"Loading..."</div>

const DefaultLayout : React.FC = () => {

  React.useEffect(() => {
    
  }, []);
  
  return (
    <div className=''>
        <HeaderLayout />
        <div className='container__dyap'>
          <Suspense fallback={"loading"}>
            <Routes>
              {
                routes.map((route, idx) => {
                  return(
                    route.element && (
                      <Route 
                        key={idx}
                        path={route.path}
                        element={<route.element />}
                      />
                    )
                  )
                })
              }
            </Routes>
          </Suspense>
        </div>
    </div>
  );
}

export default DefaultLayout;
