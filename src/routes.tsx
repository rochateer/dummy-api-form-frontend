import React from 'react';

interface Array_Routes {
    path : string;
    element : React.LazyExoticComponent<React.FC<{}>>
}

const ApiDummy = React.lazy(() => import("./views/apidummy/ApiDummy"));

const routes : Array_Routes[] = [
    {path : '/api', element : ApiDummy}
]

export default routes