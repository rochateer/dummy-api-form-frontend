import React from 'react';
import {useDispatch, useSelector} from 'react-redux';

import './login.scss';

interface StateUserManagementReducer {
  userManagementReducer : any
}

const Login : React.FC = () => {

  const reducerUser = useSelector<StateUserManagementReducer>((state) => state.userManagementReducer);
  const dispatch = useDispatch();

  React.useEffect(() => {
    console.log("reducerUser", reducerUser)
  }, []);
  
  return (
    <div className='container height-vh-100 d-flex flex-row justify-content-center align-items-center'>
        <div className='container__login d-flex flex-row justify-content-center align-items-center'>
          <div className='box__login d-flex flex-column justify-content-center'>
            <div className="header__login d-flex justify-content-center">
              Welcome to DyAF
            </div>
            <div className='box__login-input d-flex flex-row justify-content-center align-items-center'>
              <div className="form">
                <div className="form-group">
                  <label>Email</label>
                  <input type="text" className="form-control input__email" />
                </div>
                <div className="form-group">
                  <label>Password</label>
                  <input type="password" className="form-control input__password" />
                </div>
                <button className="button__login--signin btn btn-primary">Login</button>
              </div>
            </div>
          </div>
        </div>
        

    </div>
  );
}

export default Login;
