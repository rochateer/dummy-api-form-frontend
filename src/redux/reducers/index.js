import { combineReducers } from "redux";
import intialReducer from "./initialReducer";
import userManagementReducer from "./userManagement"

const rootReducer = combineReducers({
    intialReducer : intialReducer,
    userManagementReducer : userManagementReducer
})

export default rootReducer