const initialState = {
    state : {}
}

const intialReducer = (state = initialState, action) => {
    switch(action.type){
        case "INIT" :
            return {
                ...state
            }
        default: return state
    }
}

export default intialReducer;