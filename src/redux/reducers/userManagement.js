import { LOGIN_USER, LOGIN_ROLE } from "../types/actInTypes";

const initialState = {
    user_login : {}
}

const userManagementReducer = (state = initialState, action) => {
    switch(action.type) {
        case LOGIN_USER : 
            return {
                ...state,
                user_login: {...action.payload}
            }
        default: return state
    }
}

export default userManagementReducer;