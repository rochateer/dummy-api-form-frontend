import { configureStore, applyMiddleware } from '@reduxjs/toolkit';
import thunk from 'redux-thunk';
import rootReducer from "./reducers";

const initialState = {
  is_login : false
}

const appStore = configureStore({
    reducer : rootReducer, 
    middleware: [thunk]
})

export type RootState = ReturnType<typeof appStore.getState>

export default appStore;