import React, {Suspense, useEffect} from 'react';
import logo from './logo.svg';
import './scss/style.scss';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';

const DefaultPage = React.lazy(() => import('./views/defaultViews/DefaultPage'))
const Login = React.lazy(() => import('./views/login/Login'))
const DefaultLayout = React.lazy(() => import('./layout/DefaultLayout'))

const App = () => {

  const dispatch = useDispatch();

  const setReduxUserData = () => {
    dispatch(
      {
        type : "LOGIN_USER",
        payload : {
          username : 'rcthr',
          email : 'aroccha27@gmail.com'
        }
      }
    )
  }

  React.useEffect(() => {
    setReduxUserData();
  }, []);

  return (
    <BrowserRouter>
      <Suspense fallback={"..loading"} >
        <Routes>
          {/* <Route path="/" element={<DefaultPage />} /> */}
          <Route path="/login" element={<Login />} />
          <Route path="*" element={<DefaultLayout />} />
        </Routes>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
